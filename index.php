<?php
    require_once 'classes/aluno.class.php';
    require_once 'classes/curso.class.php';
    
    $a = new Aluno();
    $rsa = $a->select("SELECT a.*, c.nome as nome_curso FROM aluno a INNER JOIN curso c ON a.curso = c.id");
    
    $c = new Curso();
    $rsc = $c->select();
?>
<!DOCTYPE html>
<html>

    <head>
        <title>Avaliação JCL</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" rel="stylesheet" />
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"></script>
    </head>

    <body>
        <div class="ui container">
            <div class="ui hidden divider"></div>
            <h1 class="ui header">Matrícula de Alunos</h1>
            <!--<div class="ui form">-->
                <form class="ui form" name='matricula' method="post" action='processa.php'>
                <input type="hidden" name="id">
                <div class="fields">
                    <div class="field">
                        <label>Nome Completo</label>
                        <input type="text" name="nome" id='nome' placeholder="Seu nome">
                    </div>
                    <div class="field">
                        <label>E-mail</label>
                        <input type="email" name='email' id="email" placeholder="E-mail">
                    </div>
                    <div class="field">
                        <label>Curso</label>
                        <select name='curso' id="curso">
                            <? foreach($rsc as $row) { ?>
                            <option value="<? echo $row['id']; ?>"><? echo $row['nome']; ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <button type="button" class="ui button" id='botao' onclick="valida()">Cadastrar</button>
                </form>
            <!--</div>-->
        </div>
        <div class="ui hidden divider"></div>
        <div class="ui container">
            <div class="ui hidden divider"></div>
            <h1 class="ui header">Alunos Matriculados</h1>
            <table class="ui table">
                <thead>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Curso</th>
                    <th>Editar</th>
                    <th>Excluir</th>
                </thead>
                <tbody>
                    <? foreach ($rsa as $row){ ?>
                    <tr>
                        <td><? echo $row['id']; ?></td>
                        <td><? echo $row['nome']; ?></td>
                        <td><? echo $row['email']; ?></td>
                        <td><? echo $row['nome_curso']; ?></td>
                        <td><a href="javascript:editar(<? echo $row['id']; ?>)"><i class="pencil large icon"></i></a></td>
                        <td><a href="javascript:apagar(<? echo $row['id']; ?>)"><i class="times big red icon"></i></a></td>
                    </tr>
                    <? } ?>
                </tbody>
            </table>
        </div>
    </body>

</html>
<script>
    function valida(){
        if(document.matricula.nome.value === ''){
            alert('Favor informar seu Nome!');
            document.matricula.nome.focus();
            return false;
        } else if(document.matricula.email.value === ''){
            alert('Favor informar seu Email!');
            document.matricula.email.focus();
            return false;
        } else if(document.matricula.curso.value === ''){
            alert('Favor informar o Curso desejado!');
            document.matricula.curso.focus();
            return false;
        } else {
            var email = document.matricula.email.value;
            $.post( "json.php", { email: email }, function(rt) {
                if(rt.msg === 'erro'){
                    alert('Este email já está cadastrado!');
                } else {
                    document.matricula.submit();
                }
            }, "json");
        }
    }
    function editar(x){
        if(x !== null){
            $.post( "json.php", { action: 'upd', id: x }, function(rt) {
                if(rt.msg === 'erro'){
                    alert('Não foi possível editar esta matrícula!');
                } else {
                    document.matricula.id.value = x;
                    document.matricula.nome.value = rt.nome;
                    document.matricula.email.value = rt.email;
                    document.matricula.curso.value = rt.curso;
                    document.getElementById('botao').innerText = 'Salvar Alterações';
                }
            }, "json");
        }
    }
    function apagar(x){
        if(confirm("Voce deseja realmente excluir permanentemente o registro?")){
            $.post( "json.php", { action: 'del', id: x }, function(rt) {
                if(rt.msg === 'erro'){
                    alert('Não foi possível excluir esta matrícula!');
                } else {
                    alert('Matrícula apagada com sucesso!');
                    document.location.reload(true);
                }
            }, "json");
        }
    }
</script>