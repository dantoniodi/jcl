<?
include_once("classes/aluno.class.php");

$nome = filter_input(INPUT_POST, 'nome');
$email = filter_input(INPUT_POST, 'email');
$curso = filter_input(INPUT_POST, 'curso');
$id = filter_input(INPUT_POST, 'id');

$a = new Aluno();
$a->setNome($nome);
$a->setEmail($email);
$a->setCurso($curso);

if(empty($id)) {
    if ($a->insert()) {
        header("location: index.php?msg=addOK");
        die();
    } else {
        header("location: index.php?msg=erroAdd");
        die();
    }
} else {
    $a->setId($id);
    if ($a->update()) {
        header("location: index.php?msg=updOK");
        die();
    } else {
        header("location: index.php?id=".$id."&msg=erroUpd");
        die();
    }
}
?>