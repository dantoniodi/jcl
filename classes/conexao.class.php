<?php

class Conexao {
    
    public static $instance;
    protected $_dbOptions = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
    private $_conn;

    private function __construct() {
        try {
            $this->_conn = new PDO("pgsql:host=localhost;port=5432;dbname=jcl-teste",
                    "postgres","1234",
                    $this->_dbOptions);
            $this->_conn->setAttribute( PDO::ATTR_EMULATE_PREPARES, FALSE );
        } catch (PDOException $e) {
            echo "Erro ao conectar: " . $e->getMessage();
        }
    }
    
    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new Conexao();
        }
        return self::$instance;
    }
    
    public function getConnection() {
        return $this->_conn;
    }
    
}