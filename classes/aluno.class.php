<?php

include_once("conexao.class.php");

class Aluno {

    private $id;
    private $nome;
    private $email;
    private $curso;
    private $conn;

    public function __construct() {
        $pdo = Conexao::getInstance();
        $this->conn = $pdo->getConnection();
    }

    public function getId() {
        return $this->id;
    }
    public function getNome() {
        return $this->nome;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getCurso() {
        return $this->curso;
    }

    public function setId($param) {
        $this->id = $param;
    }
    public function setNome($param) {
        $this->nome = $param;
    }
    public function setEmail($param) {
        $this->email = $param;
    }
    public function setCurso($param) {
        $this->curso = $param;
    }

    public function select($sql = "SELECT * FROM aluno") {
        //die($sql);
        return $this->conn->query($sql);
    }

    public function selectWhere($where) {
        $sql = ("SELECT * FROM aluno WHERE " . $where);
        //die($sql);
        return $this->conn->query($sql);
    }

    public function update() {
        $sql = ("UPDATE aluno SET nome = :nome,"
                . " email = :email,"
                . " curso = :curso"
                . " WHERE id = :id");
        //die($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(":nome", $this->nome);
        $stmt->bindValue(":email", $this->email);
        $stmt->bindValue(":curso", $this->curso);
        $stmt->bindValue(":id", $this->id);
        return $stmt->execute();
    }

    public function delete() {
        $sql = ("DELETE FROM aluno WHERE id = :id");
        //die($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(":id", $this->id);
        return $stmt->execute();
    }

    public function insert() {
        $sql = ("INSERT INTO aluno (nome, email, curso)"
                . " VALUES (:nome, :email, :curso)");
        //die($sql);
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(":nome", $this->nome);
        $stmt->bindValue(":email", $this->email);
        $stmt->bindValue(":curso", $this->curso);
        return $stmt->execute();
    }

}

?>
