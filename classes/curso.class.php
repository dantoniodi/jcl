<?php

include_once("conexao.class.php");

class Curso {

    private $id;
    private $nome;
    private $conn;

    public function __construct() {
        $pdo = Conexao::getInstance();
        $this->conn = $pdo->getConnection();
    }

    public function getId() {
        return $this->id;
    }
    public function getNome() {
        return $this->nome;
    }

    public function setId($param) {
        $this->id = $param;
    }
    public function setNome($param) {
        $this->nome = $param;
    }

    public function select($sql = "SELECT * FROM curso") {
        //die($sql);
        return $this->conn->query($sql);
    }

    public function selectWhere($where) {
        $sql = ("SELECT * FROM curso WHERE " . $where);
        //die($sql);
        return $this->conn->query($sql);
    }

}

?>
