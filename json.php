<?php
    
    require_once 'classes/aluno.class.php';
    
    $id = filter_input(INPUT_POST, 'id');
    $email = filter_input(INPUT_POST, 'email');
    $action = filter_input(INPUT_POST, 'action');
    
    if(!empty($email)) { //verifica se o email ja esta cadastrado no banco
        
        $a = new Aluno();
        $rsa = $a->selectWhere("email = '".$email."'");
        if($rsa->rowCount() > 0){
            $retorno = 'erro';
        } else {
            $retorno = 'ok';
        }
        echo json_encode(array("msg"=>$retorno));
    }
    
    if(!empty($id)){ 
        
        if($action == 'upd') { //retorna dados para popular formulario na index
            $a = new Aluno();
            $rsa = $a->selectWhere("id = ".$id);

            if($rsa->rowCount() > 0){
                $row = $rsa->fetch();
                echo json_encode(array("nome"=>$row['nome'],"email"=>$row['email'],"curso"=>$row['curso']));
            } else {
                echo json_encode(array("msg"=>"erro"));
            }
        }
        
        if($action == 'del'){ //deleta id
            $a = new Aluno();
            $a->setId($id);
            if($a->delete()){
                $retorno = 'ok';
            } else {
                $retorno = 'erro';
            }
            echo json_encode(array("msg"=>$retorno));
        }
    }
?>